from django.shortcuts import render
from django.http import HttpResponse

# def home(request):
#     return render(request, 'main/home.html')

def storytiga(request):
    return render(request, 'main/index.html')

def activities(request):
    return render(request, 'main/activities.html')

def storylima(request):
    return render(request, 'storylima/schedule.html')

def storyenam(request):
    return render(request, 'storyenam/kegiatan.html')