# Generated by Django 3.1.2 on 2020-10-17 11:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=300, null=True)),
                ('lecturer', models.CharField(max_length=300, null=True)),
                ('credit', models.IntegerField(null=True)),
                ('description', models.TextField(max_length=300, null=True)),
                ('term', models.CharField(max_length=300, null=True)),
                ('classroom', models.CharField(max_length=300, null=True)),
            ],
        ),
    ]
