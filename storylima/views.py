from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Schedule
from .forms import ScheduleForm
from storylima import forms, models

# Create your views here.
def schedule(request):
    schedule = Schedule.objects.all().values()
    form = ScheduleForm(request.POST or None)
    context = {
        'schedule':schedule,
        'form':form,
    }
    if request.method == "POST":
        if form.is_valid():
            schedule.create(
                subject = form.cleaned_data.get('subject'),
                lecturer = form.cleaned_data.get('lecturer'),
                credit = form.cleaned_data.get('credit'),
                description = form.cleaned_data.get('description'),
                term = form.cleaned_data.get('term'),
                classroom = form.cleaned_data.get('classroom'),
            )
        return redirect('/storylima/list')
           
    return render(request, 'schedule.html', context)

def list_subject(request):
    schedule = Schedule.objects.all().values()
    context = {'schedule':schedule}
    if request.method == "POST":
        if 'id' in request.POST:
            Schedule.objects.get(id=request.POST['id']).delete()
            return redirect('/storylima/list')
    list_subject = Schedule.objects.all()
    return render(request, 'list_subject.html', {'schedule':list_subject})

def details_subject(request, index):
    schedule = Schedule.objects.get(pk=index)
    context = {'schedule':schedule}
    return render(request, 'details_subject.html', context)