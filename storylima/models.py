from django.db import models

# Create your models here.
class Schedule(models.Model):
    subject = models.CharField(max_length=300, null=True)
    lecturer = models.CharField(max_length=300, null=True)
    credit = models.IntegerField(null=True)
    description = models.TextField(max_length=300, null=True)
    term = models.CharField(max_length=300, null=True)
    classroom = models.CharField(max_length=300, null=True)