from django import forms
from .models import Schedule

class ScheduleForm(forms.Form):
    
    subject = forms.CharField(
        label = 'Subject', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'Subject Name'}),
    )

    lecturer = forms.CharField(
        label = 'Lecturer', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'Lecturer Name'}),
    )

    credit = forms.IntegerField(
        label = 'Credits', 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'SKS'}),
    )

    description = forms.CharField(
        label = 'Description', 
        max_length = 150, 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'Description'}),
    )

    term = forms.CharField(
        label = 'Term', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'Ex: Ganjil 2020/2021'}),
    )

    classroom = forms.CharField(
        label = 'Classroom', 
        max_length = 100, 
        required = True,
        widget = forms.TextInput(attrs ={'placeholder': 'Classroom'}),
    )