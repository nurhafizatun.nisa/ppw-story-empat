from django.urls import path
from . import views

app_name = 'storylima'

urlpatterns = [
    path('form', views.schedule, name='schedule'),
    path('list', views.list_subject, name='list_subject'),
    path('details/<int:index>', views.details_subject, name='details_subject'),
]