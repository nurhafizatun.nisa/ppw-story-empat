from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Kegiatan, Member

# Create your tests here.

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.kegiatan_url = reverse("storyenam:kegiatan")
        self.test_kegiatan = Kegiatan.objects.create(
            kegiatan = "belajar"
        )
        self.test_member = Member.objects.create(
            member = "nisa",
            kegiatan = self.test_kegiatan
        )
        self.test_delete_url = reverse("storyenam:kegiatanDelete",args=[self.test_kegiatan.id])
        self.test_member_delete_url = reverse("storyenam:memberDelete",args=[self.test_member.id])

    def test_kegiatan_GET(self):
        response = self.client.get(self.kegiatan_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "storyenam/kegiatan.html")

    def test_kegiatan_POST(self):
        response = self.client.post(self.kegiatan_url, {
            "kegiatan" : "makan"
        }, follow=True)
        self.assertContains(response, "makan")

    def test_member_POST(self):
        response = self.client.post(self.kegiatan_url, {
            "member" : "rafi",
            "kegiatan" : self.test_kegiatan.id,
        }, follow=True)
        self.assertContains(response, "rafi")

    def test_notValid_POST(self):
        response = self.client.post(self.kegiatan_url, {
            "member" : "rafi",
            "kegiatan" : "jalan",
        }, follow=True)
        self.assertContains(response, "Input tidak sesuai")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        print(response.content)
        self.assertContains(response, "Berhasil dihapus")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "Berhasil dihapus")

