from django import forms
from .models import Kegiatan, Member

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'
        widgets = {
            'activity' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Want to have fun?"}),
        }

class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = '__all__'
        widgets = {
            'member' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Want to join?"}),
        }