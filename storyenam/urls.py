from django.urls import path
from . import views

app_name = 'storyenam'

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('delete/<str:pk>', views.delete, name='kegiatanDelete'),
    path('delete/member/<str:pk>', views.deleteMember, name='memberDelete')
]