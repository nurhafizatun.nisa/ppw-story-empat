from django.shortcuts import render, redirect
from .models import Member, Kegiatan
from .forms import MemberForm, KegiatanForm
from django.contrib import messages

# Create your views here.
def kegiatan(request):
    if request.method == "POST":
        formA = KegiatanForm(request.POST)
        formB = MemberForm(request.POST)
        if formA.is_valid():
            formA.save()
            return redirect('storyenam:kegiatan')
        elif formB.is_valid():
            formB.save()
            return redirect('storyenam:kegiatan')
        else:
            return redirect('storyenam:kegiatan')
    else:
        formA = KegiatanForm()
        formB = MemberForm()
        activities = Kegiatan.objects.all()
        members = Member.objects.all()
        context = {
            'formA' : formA,
            'formB' : formB,
            'activities' : activities,
            'members' : members
        }
        return render(request, 'kegiatan.html', context)

def delete(request,pk):
    kegiatan = Kegiatan.objects.get(id=pk)
    kegiatan.delete()
    return redirect('storyenam:kegiatan')

def deleteMember(request,pk):
    member = Member.objects.get(id=pk)
    member.delete()
    return redirect('storyenam:kegiatan')
