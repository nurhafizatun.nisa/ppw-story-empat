from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    kegiatan = models.CharField(max_length=100)
    def __str__(self):
        return self.Kegiatan

class Member(models.Model):
    member = models.CharField(max_length=100)
    aktivitas = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    def __str__(self):
        return self.member